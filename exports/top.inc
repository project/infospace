<?php

$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'infospace_top';
$ad->provider = 'infospace_provider';
$ad->settings = array(
  'infospace_provider' => array(
    'placement' => 'top',
    'display_label' => 0,
    'styles' => '',
  ),
);
