<?php

/**
 * Implemenents hook_ad_manager_ad_default().
 */
function infospace_ad_manager_ad_default() {
  $ads = array();
  $files = file_scan_directory(drupal_get_path('module', 'infospace') . '/exports', '/\.inc$/');

  foreach ($files as $path => $file) {
    require $path;
    if (isset($ad)) {
      $ads[$ad->name] = $ad;
    }
  }

  return $ads;
}
