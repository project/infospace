<?php

/**
 * @file
 * Signature generator for Infospace API.
 */

// Access ID should only contain alphanumeric characters, underscores, and
// periods.
$access_id = filter_input(INPUT_GET, 'access_id', FILTER_VALIDATE_REGEXP,
  array('options' => array('regexp' => '/^[A-Za-z0-9\_\.]+$/')));
// Do not need to sanitize the query, because it doesn't get saved anywhere, or
// used in DB queries. Also, when this module calls this file, it will already
// have passed the search query through Drupal's check_plain(); if we re-run
// similar functions, I think we'll double-encode the value, which will cause
// the signature verification to fail.
$query = $_GET['query'];

if (empty($access_id) || empty($query)) {
  header('HTTP/1.0 400 Bad request');
  print json_encode(array('error' => 'Invalid access ID or query.'));
  die;
}

// Build signature unique to this time and search query.
require_once 'InfoSpaceRequestSigner.php';
$Infospace = new InfoSpaceRequestSigner($access_id);
$signature = $Infospace->getSignature($query);

header('Content-Type: application/json');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');

print json_encode(array('signature' => $signature));
die;
