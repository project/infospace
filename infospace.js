!(function($, Drupal, document) {
  'use strict';

  var doSearch = function doSearch(signature) {
    signature = signature || '';

    insp.search.doSearch({
      query: Drupal.settings.InfoSpace.query,
      accessId: Drupal.settings.InfoSpace.accessID,
      deviceSegments: { mobile: Drupal.settings.InfoSpace.mobileSegment },
      signature: signature,
      page: Drupal.settings.InfoSpace.pageNumber,
      isTest: Drupal.settings.InfoSpace.isTest ? true : false,
      adultFilter: Drupal.settings.InfoSpace.adultFilter,
      containers: Drupal.settings.InfoSpace.slots,
      onComplete: function(details) {
        if (Drupal.settings.InfoSpace.isTest) {
          console.log(details);
        }
      },
      onError: function(details) {
        if (Drupal.settings.InfoSpace.isTest) {
          console.log(details);
        }
      },
    });
  }

  var init = function init() {
    if (typeof Drupal.settings.InfoSpace === 'undefined') {
      return;
    }

    // Until signature-generation is working, we can submit requests without
    // signature.
    if (!Drupal.settings.InfoSpace.signatureRequired) {
      doSearch();
      return;
    }

    $.ajax({
      url: Drupal.settings.InfoSpace.signatureGenerator,
      dataType: 'json',
      cache: false,
      data: {
        access_id: Drupal.settings.InfoSpace.accessID,
        query: Drupal.settings.InfoSpace.query
      },
      success: function(data) {
        doSearch(data.signature);
      }
    });

  }
  init();

}) (jQuery, Drupal, this.document);
